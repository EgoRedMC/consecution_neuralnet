﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NotAOrB_NeuralNet
{
    [Serializable]
    [XmlRoot("Net")]
    public class NetInfo
    {
        [XmlElement("inLayer")]
        public LayerInfo iLayer;

        [XmlElement("outLayer")]
        public LayerInfo oLayer;

        [XmlArray("hidLayers")]
        [XmlArrayItem("hidLayer")]
        public List<LayerInfo> hLayers;

        public NetInfo() { }

        public NetInfo(Net n)
        {
            hLayers = new List<LayerInfo>();
            iLayer = new LayerInfo(n.GetInputLayer());
            oLayer = new LayerInfo(n.GetOutputLayer());
            for (int i = 0; i < n.GetHiddenLayers().Count(); i++)
                hLayers.Add(new LayerInfo(n.GetHiddenLayers()[i]));
        }
    }

    //----------
    [Serializable]
    [XmlRoot("Layer")]
    public class LayerInfo
    {
        [XmlArray("inNeurons")]
        [XmlArrayItem("inNeuron")]
        public List<NeuronInfo> iNeurons;

        [XmlArray("hidNeurons")]
        [XmlArrayItem("hidNeuron")]
        public List<NeuronInfo> hidNeurons;

        [XmlArray("outNeurons")]
        [XmlArrayItem("outNeuron")]
        public List<NeuronInfo> oNeurons;

        [XmlElement("bNeuron")]
        public NeuronInfo bNeuron = null;

        public LayerInfo() { }

        public LayerInfo(Layer l)
        {
            iNeurons = new List<NeuronInfo>();
            hidNeurons = new List<NeuronInfo>();
            oNeurons = new List<NeuronInfo>();
            for (int i = 0; i < (l.GetINeurons() == null ? 0 : l.GetINeurons().Count); i++)
                iNeurons.Add(new NeuronInfo(l.GetINeurons()[i]));
            for (int i = 0; i < (l.GetHNeurons() == null ? 0 : l.GetHNeurons().Count); i++)
                hidNeurons.Add(new NeuronInfo(l.GetHNeurons()[i]));
            for (int i = 0; i < (l.GetONeurons() == null ? 0 : l.GetONeurons().Count); i++)
                oNeurons.Add(new NeuronInfo(l.GetONeurons()[i]));
            bNeuron = new NeuronInfo(l.GetBNeuron());
        }
    }

    //----------
    [Serializable]
    [XmlRoot("Neuron")]
    public class NeuronInfo
    {
        [XmlElement("type")]
        public NeuronType type;

        [XmlArray("axons")]
        [XmlArrayItem("axon")]
        public List<AxonInfo> inputAxons;

        [XmlElement("number")]
        public int number;

        public NeuronInfo() { }

        public NeuronInfo(Neuron n)
        {
            if (n == null)
                return;
            inputAxons = new List<AxonInfo>();
            type = n.GetNeuronType();
            number = n.GetNeuronNum();
            if ((n.GetNeuronType() == NeuronType.H) || (n.GetNeuronType() == NeuronType.O))
            for (int i = 0; i < (n.GetAxons().Count); i++)
                inputAxons.Add(new AxonInfo(n.GetAxons()[i]));
        }
    }

    //----------
    [Serializable]
    [XmlRoot("Axon")]
    public class AxonInfo
    {
        [XmlElement("weight")]
        public double weight;

        [XmlElement("inNeuronNum")]
        public int inputNeuronNum;

        [XmlElement("outNeuronNum")]
        public int outputNeuronNum;

        public AxonInfo() { }

        public AxonInfo(Axon a)
        {
            weight = a.Getweight();
            inputNeuronNum = a.GetInNeuron().GetNeuronNum();
            outputNeuronNum = a.GetOutNeuron().GetNeuronNum();
        }
    }
}
