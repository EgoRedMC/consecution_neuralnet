﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;


namespace NotAOrB_NeuralNet
{

    public class Program
    {
        public static Random R = new Random();
        const int N = 3;

        private static Net MakeNetINeed()
        {
            Net WTF;
            WTF = new Net();
            WTF.setNumberHiddenLayers(1);
            WTF.setNumderHiddenNeuronsOnAllLayers(3);
            WTF.setNumberInputNeurons(2);
            WTF.setNumberOutputNeurons(1);
            WTF.AddBiasNeurons();
            WTF.connectNeurons();
            return WTF;
        }

        private static double countError(List<double> ress)
        {
            double err = 0;
            for (int i = 0; i < ress.Count; i++)
            {
                err += Math.Pow(((i%4==2?0:1) - ress[i]),2);
            }
            err /= ress.Count;

            return err;
        }

        static void Main(string[] args)
        {
            Net N = Net.Load("1.xml");
            double error = 0;
            List<double> results = new List<double>();
            //
            //ОБУЧЕНИЕ
            //
            for (int i = 0; i < 1; i++)
            {

                //
                results.Add(N.Work(new double[] { 0, 0 })[0]);
                error = countError(results);
                Console.WriteLine(results[results.Count-1] +" " +error);
                N.Save("1.xml");
                //
                results.Add(N.Work(new double[] { 0, 1 })[0]);
                error = countError(results);
                Console.WriteLine(results[results.Count - 1] + " " + error);
                N.Save("1.xml");
                //
                results.Add(N.Work(new double[] { 1, 0 })[0]);
                error = countError(results);
                Console.WriteLine(results[results.Count - 1] + " " + error);
                N.Save("1.xml");
                //
                results.Add(N.Work(new double[] { 1, 1 })[0]);
                error = countError(results);
                Console.WriteLine(results[results.Count - 1] + " " + error);
                N.Save("1.xml");
            }
        }
    }
}
