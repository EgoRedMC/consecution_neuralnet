﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotAOrB_NeuralNet
{
    public class Layer
    {
        #region fields
        protected List<InputNeuron> INeurons = new List<InputNeuron>();
        protected List<HiddenNeuron> HNeurons = new List<HiddenNeuron>();
        protected List<OutputNeuron> ONeurons = new List<OutputNeuron>();
        protected BiasNeuron BNeuron = null;
        #endregion

        public Layer() { }

        public List<HiddenNeuron> GetHNeurons()
        {
            return HNeurons;
        }

        public List<InputNeuron> GetINeurons()
        {
            return INeurons;
        }

        public BiasNeuron GetBNeuron()
        {
            return BNeuron;
        }

        public List<OutputNeuron> GetONeurons()
        {
            return ONeurons;
        }
    }

    public class InputLayer : Layer
    {
        public InputLayer()
        {
            ONeurons = null;
            HNeurons = null;
        }

        public void AddInNeuron(InputNeuron N)
        {
            INeurons.Add(N);
        }

        public void AddBNeuron(int num)
        {
            BNeuron = new BiasNeuron(num);
        }

        public void Work(double[] inputs)
        {
            for (int i = 0; i < (inputs.Length > INeurons.Count ? INeurons.Count : inputs.Length); i++)
                INeurons[i].SetInput(inputs[i]);
        }

    }

    public class HiddenLayer : Layer
    {
        public HiddenLayer()
        {
            INeurons = null;
            ONeurons = null;
        }

        public void AddHidNeuron(HiddenNeuron N)
        {
            HNeurons.Add(N);
        }

        public void AddBNeuron(int num)
        {
            BNeuron = new BiasNeuron(num);
        }

        public void Work()
        {
            for (int i = 0; i < HNeurons.Count; i++)
                HNeurons[i].SetOutput();
        }
    }

    public class OutputLayer : Layer
    {
        public OutputLayer()
        {
            INeurons = null;
            HNeurons = null;
        }

        public void AddOutNeuron(OutputNeuron N)
        {
            ONeurons.Add(N);
        }

        public double[] Work()
        {
            List<double> returned = new List<double>();
            //
            for (int i = 0; i < ONeurons.Count; i++)
            {
                ONeurons[i].SetOutput();
                returned.Add(ONeurons[i].GetOutput());
            }
            //
            return returned.ToArray();
        }

        public void AddBNeuron(int num)
        {
            BNeuron = new BiasNeuron(num);
        }
    }

}
