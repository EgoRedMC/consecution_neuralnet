﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotAOrB_NeuralNet
{
    public class Axon
    {
        protected readonly Neuron InNeuron;
        protected readonly Neuron OutNeuron;
        protected double weight;

        public Axon(Neuron InNeuronI, Neuron OutNeuronI)
        {
            InNeuron = InNeuronI;
            OutNeuron = OutNeuronI;
            weight = Program.R.NextDouble();
        }

        public Axon(Neuron InNeuronI, Neuron OutNeuronI, double w)
        {
            InNeuron = InNeuronI;
            OutNeuron = OutNeuronI;
            weight = w;
        }

        public Neuron GetInNeuron()
        {
            return InNeuron;
        }

        public Neuron GetOutNeuron()
        {
            return OutNeuron;
        }

        public double Getweight()
        {
            return weight;
        }
    }
}
