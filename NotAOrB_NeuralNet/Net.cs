﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace NotAOrB_NeuralNet
{
    public class Net
    {
        #region fields
        private InputLayer inputLayer;
        private OutputLayer outputLayer;
        private List<HiddenLayer> hiddenLayers;
        /// <summary>
        /// кол-во связей между нейронами
        /// </summary>
        private int AxonNumber = 0;
        /// <summary>
        /// переменная для нумерования нейронов для их связи
        /// </summary>
        private int NeuronNumber = 0;
        #endregion

        public Net()
        {
            inputLayer = new InputLayer();
            outputLayer = new OutputLayer();
            hiddenLayers = new List<HiddenLayer>();
        }

        public Net(NetInfo _netInfo)
        {
            //
            inputLayer = new InputLayer();
            outputLayer = new OutputLayer();
            hiddenLayers = new List<HiddenLayer>();
            //
            generateInputLayer(_netInfo.iLayer);
            generateHiddenLayers(_netInfo);
            generateOutputLayer(_netInfo.oLayer);
            connectNeurons(_netInfo);
        }

        #region функции создания сети из сохранения
        private void generateInputLayer(LayerInfo _l)
        {
            for (int i = 0; i < _l.iNeurons.Count; i++)
                inputLayer.AddInNeuron(new InputNeuron(_l.iNeurons[i].number));
            if (_l.bNeuron != null)
                inputLayer.AddBNeuron(_l.bNeuron.number);
        }

        private void generateHiddenLayers(NetInfo _n)
        {
            for (int i = 0; i < _n.hLayers.Count; i++)
            {
                LayerInfo _l = _n.hLayers[i];
                HiddenLayer l = new HiddenLayer();
                if (_l.bNeuron != null)
                    l.AddBNeuron(_l.bNeuron.number);
                for (int j = 0; j < _l.hidNeurons.Count; j++)
                    l.AddHidNeuron(new HiddenNeuron(_l.hidNeurons[j].number));
                //
                hiddenLayers.Add(l);
            }
        }

        private void generateOutputLayer(LayerInfo _l)
        {
            for (int i = 0; i < _l.oNeurons.Count; i++)
                outputLayer.AddOutNeuron(new OutputNeuron(_l.oNeurons[i].number));
            if (_l.bNeuron != null)
                outputLayer.AddBNeuron(_l.bNeuron.number);
        }

        private void connectNeurons(NetInfo _n)
        {
            //ГОСПОДИ, ЧТО ЭТО????!!!!
            for (int j = 0; j < hiddenLayers.Count; j++)
                for (int i = 0; i < hiddenLayers[j].GetHNeurons().Count; i++)
                    for (int k = 0; k < _n.hLayers[j].hidNeurons[i].inputAxons.Count; k++)
                        hiddenLayers[j].GetHNeurons()[i].ConnectWithAxon(findNeuronByNumber(_n.hLayers[j].hidNeurons[i].inputAxons[k].inputNeuronNum, this), _n.hLayers[j].hidNeurons[i].inputAxons[k].weight);
            for (int i = 0; i < outputLayer.GetONeurons().Count; i++)
                for (int k = 0; k < _n.oLayer.oNeurons[i].inputAxons.Count; k++)
                    outputLayer.GetONeurons()[i].ConnectWithAxon(findNeuronByNumber(_n.oLayer.oNeurons[i].inputAxons[k].inputNeuronNum, this), _n.oLayer.oNeurons[i].inputAxons[k].weight);
        }

        private Neuron findNeuronByNumber(int num, Net n)
        {
            if (n.GetInputLayer().GetBNeuron().GetNeuronNum() == num)
                return n.GetInputLayer().GetBNeuron();
            for (int i = 0; i < n.GetInputLayer().GetINeurons().Count; i++)
                if (n.GetInputLayer().GetINeurons()[i].GetNeuronNum() == num)
                    return n.GetInputLayer().GetINeurons()[i];
            //
            if (n.GetOutputLayer().GetBNeuron().GetNeuronNum() == num)
                return n.GetOutputLayer().GetBNeuron();
            for (int i = 0; i < n.GetOutputLayer().GetONeurons().Count; i++)
                if (n.GetOutputLayer().GetONeurons()[i].GetNeuronNum() == num)
                    return n.GetOutputLayer().GetONeurons()[i];
            //
            for (int j = 0; j < n.GetHiddenLayers().Count; j++)
            {
                if (n.GetHiddenLayers()[j].GetBNeuron().GetNeuronNum() == num)
                    return n.GetHiddenLayers()[j].GetBNeuron();
                for (int i = 0; i < n.GetHiddenLayers()[j].GetHNeurons().Count; i++)
                    if (n.GetHiddenLayers()[j].GetHNeurons()[i].GetNeuronNum() == num)
                        return n.GetHiddenLayers()[j].GetHNeurons()[i];
            }
            //
            throw (new ArgumentOutOfRangeException());
        }
        #endregion

        #region функции настройки информации о сети
        /// <summary>
        /// установление кол-ва входных нейронов
        /// </summary>
        /// <param name="n"></param>
        public void setNumberInputNeurons(int n)
        {
            for (int i = 0; i < n; i++)
                inputLayer.AddInNeuron(new InputNeuron(NeuronNumber++));
        }

        /// <summary>
        /// установление кол-ва скрытых слоев
        /// </summary>
        /// <param name="n"></param>
        public void setNumberHiddenLayers(int n)
        {
            for (int i = 0; i < n; i++)
                hiddenLayers.Add(new HiddenLayer());
        }

        /// <summary>
        /// установление кол-ва нейронов на скрытом слое
        /// </summary>
        /// <param name="n"></param>
        public void setNumberHiddenNeuronsOnLayer(int layerNumber, int n)
        {
            if ((n < hiddenLayers.Count) && (n >= 0))
                for (int i = 0; i < n; i++)
                    hiddenLayers[layerNumber].AddHidNeuron(new HiddenNeuron(NeuronNumber++));
            else
                throw (new IndexOutOfRangeException());
        }

        /// <summary>
        /// установление кол-ва нейронов на всех скрытых слоях
        /// </summary>
        /// <param name="n"></param>
        public void setNumderHiddenNeuronsOnAllLayers(int n)
        {
            for (int i = 0; i < hiddenLayers.Count; i++)
                for (int j = 0; j < n; j++)
                    hiddenLayers[i].AddHidNeuron(new HiddenNeuron(NeuronNumber++));
        }

        /// <summary>
        /// добавление bias нейронов
        /// </summary>
        public void AddBiasNeurons()
        {
            inputLayer.AddBNeuron(NeuronNumber++);
            for (int i = 0; i < hiddenLayers.Count; i++)
                hiddenLayers[i].AddBNeuron(NeuronNumber++);
            outputLayer.AddBNeuron(NeuronNumber++);
        }

        /// <summary>
        /// установление кол-ва выходных нейронов
        /// </summary>
        /// <param name="n"></param>
        public void setNumberOutputNeurons(int n)
        {
            for (int i = 0; i < n; i++)
                outputLayer.AddOutNeuron(new OutputNeuron(NeuronNumber++));
        }

        /// <summary>
        /// установление кол-ва аксонов к каждому нейрону от предыдущего слоя
        /// (не работает и пока не будет)
        /// </summary>
        /// <param name="n"></param>
        public void setAxonNumber(int n)
        {
            //AxonNumber = n;
        }
        #endregion


        /// <summary>
        /// соединяет нейроны соседних слоёв друг с другом
        /// (жопа, а не функция, притом ещё и не вся)
        /// </summary>
        public void connectNeurons()
        {
            if (AxonNumber == 0) //каждый с каждым
            {
                for (int i = 0; i < hiddenLayers[0].GetHNeurons().Count; i++)
                {
                    for (int j = 0; j < inputLayer.GetINeurons().Count; j++)
                        hiddenLayers[0].GetHNeurons()[i].ConnectWithAxon(inputLayer.GetINeurons()[j]);
                    if (inputLayer.GetBNeuron() != null)
                        hiddenLayers[0].GetHNeurons()[i].ConnectWithAxon(inputLayer.GetBNeuron());
                }
                //if (hiddenLayers.Count > 0)
                for (int k = 1; k < hiddenLayers.Count; k++)
                    for (int i = 0; i < hiddenLayers[k].GetHNeurons().Count; i++)
                    {
                        for (int j = 0; j < hiddenLayers[k - 1].GetHNeurons().Count; j++)
                            hiddenLayers[k].GetHNeurons()[i].ConnectWithAxon(hiddenLayers[k - 1].GetHNeurons()[j]);
                        if (hiddenLayers[k - 1].GetBNeuron() != null)
                            hiddenLayers[k].GetHNeurons()[i].ConnectWithAxon(hiddenLayers[k - 1].GetBNeuron());
                    }
                for (int i = 0; i < outputLayer.GetONeurons().Count; i++)
                {
                    for (int j = 0; j < hiddenLayers[hiddenLayers.Count - 1].GetHNeurons().Count; j++)
                        outputLayer.GetONeurons()[i].ConnectWithAxon(hiddenLayers[hiddenLayers.Count - 1].GetHNeurons()[j]);
                    if (hiddenLayers[hiddenLayers.Count - 1].GetBNeuron() != null)
                        outputLayer.GetONeurons()[i].ConnectWithAxon(hiddenLayers[hiddenLayers.Count - 1].GetBNeuron());
                }
            }

        }

        /// <summary>
        /// запускает непосредственно работу сети
        /// </summary>
        public double[] Work(double[] inputs)
        {
            inputLayer.Work(inputs);
            for (int i = 0; i < hiddenLayers.Count; i++)
                hiddenLayers[i].Work();
            return outputLayer.Work();
        }

        //-------

        public Layer GetInputLayer()
        {
            return inputLayer;
        }

        public Layer GetOutputLayer()
        {
            return outputLayer;
        }

        public List<HiddenLayer> GetHiddenLayers()
        {
            return hiddenLayers;
        }

        public void Save(string filename)
        {
            NetInfo NI = new NetInfo(this);
            XmlSerializer saver = new XmlSerializer(typeof(NetInfo));
            using (StreamWriter stream = new StreamWriter(filename))
                saver.Serialize(stream, NI);
        }

        public static Net Load(string filename)
        {
            NetInfo NI = new NetInfo();
            using (var stream = new StreamReader("1.xml"))
            {
                var reader = new XmlSerializer(typeof(NetInfo));
                NI = reader.Deserialize(stream) as NetInfo;
            }
            return new Net(NI);
        }
    }
}
