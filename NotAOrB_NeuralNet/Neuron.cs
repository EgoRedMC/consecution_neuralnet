﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NotAOrB_NeuralNet
{

    public enum NeuronType
    {
        H,
        I,
        O,
        B
    }

    public class Neuron
    {
        #region fields
        protected NeuronType type;
        protected List<Axon> InputAxons = new List<Axon>();
        protected double Input;
        protected double Output;
        protected int Number;
        #endregion

        public Neuron(int number)
        {
            Input = 0;
            Number = number;
        }

        public double GetOutput()
        {
            return Output;
        }

        public NeuronType GetNeuronType()
        {
            return type;
        }

        public int GetNeuronNum()
        {
            return Number;
        }

        public List<Axon> GetAxons()
        {
            return InputAxons;
        }
    }

    //

    public class InputNeuron : Neuron
    {
        public InputNeuron(int number) : base(number)
        {
            type = NeuronType.I;
            InputAxons = null;
        }

        public void SetInput(double d)
        { Input = d; Output = Input; }
    }

    public class BiasNeuron : Neuron
    {
        public BiasNeuron(int number) : base(number)
        {
            Input = 1;
            Output = 1;
            type = NeuronType.B;
            InputAxons = null;
        }
    }

    public class HiddenNeuron : Neuron
    {
        public HiddenNeuron(int number) : base(number)
        {
            type = NeuronType.H;
        }

        public void ActivationFunc()
        {
            Output = 1 / (1 + Math.Pow(Math.E, -Input));
        }

        public void GetInput()
        {
            for (int i = 0; i < InputAxons.Count; i++)
                if (InputAxons[i].GetInNeuron().GetNeuronType() != NeuronType.B)
                    this.Input += InputAxons[i].GetInNeuron().GetOutput() * InputAxons[i].Getweight();
        }

        public void SetOutput()
        {
            GetInput();
            ActivationFunc();
            //
            for (int i = 0; i < InputAxons.Count; i++)
                if (InputAxons[i].GetInNeuron().GetNeuronType() == NeuronType.B)
                    Output += InputAxons[i].Getweight();
        }

        public void ConnectWithAxon(Neuron N)
        { InputAxons.Add(new Axon(N, this)); }

        public void ConnectWithAxon(Neuron N, double weight)
        { InputAxons.Add(new Axon(N, this, weight)); }
    }

    public class OutputNeuron : Neuron
    {
        public OutputNeuron(int number) : base(number)
        {
            Input = 0;
            type = NeuronType.O;
        }

        public void ActivationFunc()
        {
            Output = 1 / (1 + Math.Pow(Math.E, -Input));
        }

        public void SetOutput()
        {
            GetInput();
            ActivationFunc();
            //
            for (int i = 0; i < InputAxons.Count; i++)
                if (InputAxons[i].GetInNeuron().GetNeuronType() == NeuronType.B)
                    Output += InputAxons[i].Getweight();
        }

        public void GetInput()
        {
            for (int i = 0; i < InputAxons.Count; i++)
                if (InputAxons[i].GetInNeuron().GetNeuronType() != NeuronType.B)
                    this.Input += InputAxons[i].GetInNeuron().GetOutput() * InputAxons[i].Getweight();
        }

        public void ConnectWithAxon(Neuron N)
        { InputAxons.Add(new Axon(N, this)); }

        public void ConnectWithAxon(Neuron N, double weight)
        { InputAxons.Add(new Axon(N, this, weight)); }
    }


}
